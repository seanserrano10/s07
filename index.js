/*
	Learning Objectives:
		- create code that will execute only when a given condition has been made

	Topics:
		- Assignment Operators
		- Conditional Statement
		- Ternary Operators

	Lesson Proper

	Conditional Statements
		- a conditional statement is one of the key features of a programming language

		Example: 
			is the container full or not?
			is the temperature higher than or equal to 40 degrees celcius?
			does the title contain an ampersand (&) character?

	There are three types of conditional statements:
	1. if-else statement
	2. switch statement
	3. try-catch-finally statements

	Operators
		- allow programming languages to execute operations or evaluations

	[SECTION] Assignment Operators
		- assign a value to a variable

		Basic Assignment Operator (=)
		- it allows us to assign a value to a variable
*/

let variable = "initial value";

/*
	Mathematical Operators
		Addition (+)
		Subtraction (-)
		Multiplication (*)
		Division (/)
		Modulo (%)

	Whenever you use a mathematical operator, a value is returned. It is only up to us if we save that returned value.

	+, -, *, / operators allow us to assign the result of the operation to the value of the left operand.
*/

let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

/*
	Addition Assignment Operator (+=):
		left operand
		- is the variable or value of the left side of the operator

		right operand
		- is the variable or value of the right side of the operator

	Ex. sum = num1 + num 4
		- reassigned the value of sum with the result of num1 + num4
*/

let sum1 = num1 + num4;

// num1 = num1 + num4;
num1 += num4;
console.log(num1);

num2 += num3;
console.log(num2);

num1 += 55;
console.log(num1);
console.log(num4);		//40 - previous right should not be affected

let string1 = "Boston";
let string2 = "Celtics";

//string1 = string1 + string2
string1 += string2;
console.log(string1);	//BostonCeltics - concatenation of strings
console.log(string2);	//Celtics

//15 += num1;		produces an error because we do not use assignment operator when the left operand is just a value/data

/*
	Subtraction Assignment Operator (-=)
*/

num1 -= num2;
console.log("Result of subtraction assignment operator: " + num1);

/*
	Multiplication Assignment Operator (*=)
*/

num2 *= num3;
console.log("Result of multiplication assignment operator: " + num2);

/*
	Diviosion Assignment Operator (/=)
*/

num4 /= num3;
console.log("Result of division assignment operator: " + num4);

/*
	[SECTION] Arithmetic Operators
*/

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = y - x;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = y / x;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulus operator: " + remainder);

/*
	[SECTION] Multiple Operators and Parenthesis

	When multiple operators are applied in a single statement, it follows the PEMDAS rule

	The operations are done in the following order:
	expression = 1 + 2 - 3 * 4 / 5;
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of multiple operations: " + mdas);

//The order of operations can be changed by adding parentheses to the logic
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of PEMDAS operations: " + pemdas);

/*
	By adding (), the order of operations are changed, prioritizing operations inside the () first then follow MDAS rule

	The operations are done in the following order:
	expression = 1 + 2 - 3 * 4 / 5;
	1. 4 / 5 = 0.8
	2. 2 - 3 = -1
	3. -1 * 0.8 = 3
	4. 1 + -0.8 = 0.19
*/

let pemdas2 = (1 + (2 - 3)) * (4 / 5);
console.log("Result of 2nd PEMDAS operations example: " + pemdas2);

/*
	Increment and Decrement
		- is adding or subtracting 1 from the variable and re-assigning the new value to the variable where the increment or decrement was used

	2 Types of Incrementation: 
	1. Prefix
	2. Postfix
*/

let z = 1;

// Pre-fix Incrementation
++z;

console.log("Pre-fix Increment: " + z);

// Post-fix Incrementation
z++;

console.log("Post-fix Increment: " + z);	//3 - the value of z was added with 1

//Pre-fix vs. Post-fix Incrementation

console.log(z++);	//3 - with post-incrementation, the previous value of the value is returned first before the actual incrementation 
console.log(z);		//4 - new value is now returned
console.log(++z);	//5

//Pre-fix vs. Post-fix Decrementation
console.log(--z);	//4
console.log(z--);	//4
console.log(z);		//3

/*
	[SECTION] Comparison Operators
	- are used to compare the values of the left and right operands
	- comparison operators return boolean
		- equality or loose equality operator (==)
		- strict equality operator (===)
*/

// Loose Equality (===)	
console.log(1 == 1);			//true
	
let isSame = 55 == 55;			//true
console.log(isSame);
	
console.log(1 == '1');			//true - loose equality operator priority is the sameness of the value because with loose equality operator, forced coercion is done before comparison
console.log(0 == false);		//true - with forced coercion, false was converted to a number
console.log(1 == true);			//true
console.log("false" == false);	//false
console.log(true == "true");	//false

/*
	With loose comparison operator (==), values are compared and if operands do not have the same types, it will be forced coerced/type coerce before comparison value

	If either the operand is a number or boolean, the operands are converted into number
*/

console.log(true == "1");	//true - true is coerced into a number = 1, "1" is converted into a number 1; 1 == 1

// Strict Equality (===)
console.log(true === "1");	//false - checks both value and type
console.log(1 === "1");		//false - operands have the same value but different types

console.log("BTS" === "BTS");	//true - same value same type
console.log("Marie" === "marie");	//false - left operand is capitalized, right operand is small caps


/* 
	Inequality Operators (!=)
	
		Loose inequality operators
			- checks whether the operands are NOT equal or have different values
			- will do type coercion if the operands have different types
*/

console.log('1' != 1);	//false - both operands were converted to numbers
console.log("James" != "John");
console.log(1 != true);	//false - both operands were converted 			

/*
	Strict inequality operators (!==)
		- checks whether the two operands have different values and will check if they have different types
*/

console.log("5" !== 5);	//true - operands have different types
console.log(5 !== 5);	//false - same value same type

let name1 = "Jin";
let name2 = "Jimin";
let name3 = "Jungkook";
let name4 = "V";

let number1 = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log("-----------------------------")
console.log(numString1 == number1);
console.log(numString1 === number1);
console.log(numString1 != number1);
console.log(name4 !== "num3");
console.log(name3 == "Jungkook");
console.log(name1 === "Jin");

/*
	Relational comparison operators
		- a comparison operator compares its operand and returns a boolean value based on whether the comparison is true
*/

let a = 500;
let b = 700;
let c = 8000;
let numString3 = "5500";

console.log("-----------------------------")

// Greater than (>)
console.log(a > b);					//false
console.log(c > y);					//true

// Less than (<)
console.log(c < a);					//false
console.log(b < b);					//false
console.log(a < 1000);				//true
console.log(numString3 < 1000);		//false - forced coercion
console.log(numString3 < 6000);		//true
console.log(numString3 < "Jose");	//true - "5500" < "Jose" that is erratic (unpredictable)

// Greater than or equal to (>=)
console.log(c >= 10000);			//false
console.log(b >= a);				//true

// Less than or equal to (<=)
console.log(a <= b);				//true
console.log(c <= a);				//false

/* 
	Logical Operators

		AND operator (&&)
			- both operands on the left and right or all operands must be true otherwise false

		T && T = true
		T && F = false
		F && T = false
		F && F = false
*/

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log("-----------------------------");
console.log(authorization1);		//false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);		//true

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3);		//false

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4);		//true

let userName = "gamer2001";
let userName2 = "shadow1991";
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;
// .length is a property of string which determines the number of characters in the string
console.log(registration1);			//false - does not meet the required age

let registration2 = userName2.length > 8 && userAge2 >= requiredAge; 
console.log(registration2);			//true

/*
	OR operator (|| - double pipe)
		- returns true if atleast one of the operands are true

	T || T = true
	T || F = true
	F || T = true
	F || F = false
*/

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
console.log(guildRequirement1);		//false

let guildRequirement2 = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement2);		//true

let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement3);

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);			//false

/*
	NOT operator (!)
		- it turns a boolean into the opposite value
*/

let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin2);

console.log(!isRegistered);			//false


/*
	[SECTION] Conditional

	if-else statement
		- will run a block of code if the condition specified is true or results to true
*/

let userName3 = "c";
let userLevel3 = "25";
let userAge3 = "20";

if(userName3.length > 10){
	console.log("Welcome to Game Online!");
};

if(userLevel3 >= requiredLevel){
	console.log("You are qualified to join the guild.");
};

// else statement will be run if the condition given is false
if(userName3.length > 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the Noobies Guild.");
} else {
	console.log("You are too strong to be a noob. :(");
};

// else-if - executes a statement if the previous condition is false but another specified condition resulted to true

if(userName3.length >= 10 && userLevel3 <= 25 && userAge >= requiredAge){
	console.log("Thank you for joining the Noobies Guild.");	
} else if (userLevel3 > 25){
	console.log("You are too strong to be a noob.");
} else if (userAge3 < requiredAge){
	console.log("You are too young to join the guild.");
} else if (userName3.length < 10){
	console.log("Username too short.");
};

// if-else in function
function addNum(num1, num2){
	// check if the arguments being passed are number types
	// typeof keyword returns a string which tells the type of data that follows it
	if(typeof num1 === 'number' && typeof num2 === 'number'){
		console.log("Run only if both arguments pass our number types.");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers.");
	};
};

addNum(5, 2);
addNum("5", 2);

console.log("-----------------------------")
function login(username, password){
	if(typeof username === 'string' && typeof password === 'string'){
		console.log("Both arguments are strings.");
		/*Nested if-else
			- will run if the parent if statement is able to agree to accomplish its condition
		*/
		if(password.length < 8 && username.length < 8){
			console.log("Credentials too short.")
		} else if(username.length < 8){
			console.log("Username is too short.");
		} else if(password.length < 8){
			console.log("Password is too short.")
		} else {
			console.log("Login successful.");
		};
	} else {
		console.log("Username and/or password is not a string.")
	};
};

login("app_user1", "password123");
login("app_user1", "123");
login("user1", "password123");
login("user1", "123");
login(1, "123");

/*Mini-Activity 2 | Serrano, Sean Patrick D. | BCS21
function colorOfTheDay(day){
	if(typeof day === 'string'){
		if(day.toLowerCase() == "monday"){
			alert("Today is Monday. Wear Black.");
		} else if(day.toLowerCase() == "tuesday"){
			alert("Today is Tuesday. Wear Green.");
		} else if(day.toLowerCase() == "wednesday"){
			alert("Today is Wednesday. Wear Yellow.");
		} else if(day.toLowerCase() == "thursday"){
			alert("Today is Thursday. Wear Violet.");
		} else if(day.toLowerCase() == "friday"){
			alert("Today is Friday. Wear Violet.");
		} else if(day.toLowerCase() == "saturday"){
			alert("Today is Saturday. Wear Blue.");
		} else if(day.toLowerCase() == "sunday"){
			alert("Today is Sunday. Wear White.");
		} else {
			alert("Invalid input. Enter a valid day of the week.");
		};
	} else {
		alert("Invalid Input. Please input a string.");
	};
};

colorOfTheDay("Monday");
colorOfTheDay("WEDNESDAY");
colorOfTheDay("sUnDaY");
colorOfTheDay("Day");
colorOfTheDay(7);
*/


/*
	Switch Statement
	- an alternative to an if, else-if or else tree. Where the data being evaluated or checked in an expected input
	- if we want to select one of many code blocks/statements to be executed

	Syntax: 
	swtich(expression/condtion){
		case value:
			statement;
			break;
		default:
			statement;
			break;
*/

let hero = "Anpanman";

switch(hero){
	case "Jose Rizal":
		console.log(`Philippines National Hero`);
		break;
	case "George Washington":
		console.log(`Hero of the American Revolution`);
		break;
	case "Hercules":
		console.log(`Legendary Hero of the Greek`);
		break;
	case "Anpanman":
		console.log(`Superhero!`)
		break;
};

function roleChecker(role){
	switch(role){
		case "Admin":
			console.log("Welcome Admin, to the Dashboard.");
			break;
		case "User":
			console.log("You are not authorized to view this page.");
			break;
		case "Guest":
			console.log("Go to the registration page to register.");
			break;
		//break - it terminates the code block. If this was not added to your case, then the next case will run as well
		default:
			console.log("Invalid role.");
		//By default your switch ends with default case. So therefore, even if there is no break keyword in your default case, it will not run anything else
	};
};

roleChecker("Admin");	//Welcome Admin, to the Dashboard.



// Mini-activity | Serrano, Sean Patrick D. | BCS21

function colorOfTheDay(day){
	if(typeof day === 'string'){
		switch(day.toLowerCase()){
			case "monday":
				alert("Today is Monday. Wear Black.");
				break;
			case "tuesday":
				alert("Today is Tuesday. Wear Green.");
				break;
			case "wednesday":
				alert("Today is Wednesday. Wear Yellow.");
				break;
			case "thursday":
				alert("Today is Thursday. Wear Violet.");
				break;
			case "friday":
				alert("Today is Friday. Wear Violet.");
				break;
			case "saturday":
				alert("Today is Saturday. Wear Blue.");
				break;
			case "sunday":
				alert("Today is Sunday. Wear White.");
				break;
			default:
				alert("Invalid input. Enter a valid day of the week.");
			};
	} else {
		alert("Invalid input. Please input a string.");
	};
};

//colorOfTheDay("Wednesday");


/* Mini-activity | Serrano, Sean Patrick D. | BCS21
function gradeEvaluator(grade){
	if(typeof grade === 'number'){
		if (grade >= 90){
			console.log("Your grade is A.");
		} else if (grade >= 80){
			console.log("Your grade is B.");
		} else if (grade >= 71){
			console.log("Your grade is C.");
		} else {
			console.log("Your grade is F.");
		};
	} else {
		console.log("Input a valid grade.")
	};
};

gradeEvaluator(70);
gradeEvaluator(75);
gradeEvaluator(85);
gradeEvaluator(95);
gradeEvaluator("a");
*/


function gradeEvaluator(grade){
	if(typeof grade === 'number'){
		if (grade >= 90){
			return "A";
		//return keyword can be used in an if-else statement inside a function
		// - it allows the function to return a value
		} else if (grade >= 80){
			return "B";
		} else if (grade >= 71){
			return "C";
		} else if (grade <= 70){
			return "F";
		} else {
			return "Invalid";
		};
	} else {
		console.log("Input a valid grade.");
	};
};

let grade = gradeEvaluator(75);
console.log(`The result is: ${grade}`);		//The result is: C


/*
	Ternary Operators
		- a shorthand way of writing if-else statements

	Syntax:
	condition ? if-statement : else statement
*/

let price = 5000;

price > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000");

let hero1 = "Goku";

hero1 === "Vegeta" ? console.log("You are the Prince of all Saiyans.") : console.log("You are not even royalty.");

//2nd method

let villain = "Harvey Dent";

villain === "Two Faces"
? console.log("You live long enough to be a villain.")
: console.log("Not quite villainous yet.");

let robin = "Dick Grayson";
let currentRobin = "Tim Drake";

let isFirstRobin = currentRobin === robin
? true
: false;
console.log(isFirstRobin);		//false


let number = 10;
 
number === 5
? console.log("A")
: (number === 10 ? console.log("A is 10 ") : console.log("A is not 5 or 10"));













