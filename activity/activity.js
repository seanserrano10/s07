// Serrano, Sean Patrick D. | BCS21

function oddEvenChecker(num){
	if (typeof num === "number"){
		if(num % 2 == 0){
			console.log("The number is even.");
		} else {
			console.log("The number is odd.");
		};
	} else {
		alert("Invalid Input.");
	};
};

function budgetChecker(num){
	if (typeof num == "number"){
		if(num > 40000){
			console.log("You are over the budget.");
		} else {
			console.log("You have resources left.");
		};
	} else {
		alert("Invalid Input");
	};
};

oddEvenChecker(4);
oddEvenChecker(7);
oddEvenChecker(120);

budgetChecker(10500);
budgetChecker(50000);